﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Security
{
    public static partial class PermissionSystemNames
    {
        public static class Systems
        {
            public static class Departments
            {
                public const string Search = "SearchDepartment";
                public const string Create = "CreateDepartment";
                public const string Update = "UpdateDepartment";
                public const string Delete = "DeleteDepartment";
            }
        }

        public static class SalesOrder
        {
            public static class ManageProducts
            {
                public const string Search = "SearchDepartment";
                public const string Create = "CreateDepartment";
                public const string Update = "UpdateDepartment";
                public const string Delete = "DeleteDepartment";
            }
        }

        public static class Catalog
        {
            public const string ManageProducts = "Catalog.ManageProducts";
            public const string UserRole = "System.ManageProducts";
            public const string User = "System.ManageProducts";
            public const string Permission = "System.ManageProducts";
            public const string System = "System.ManageProducts";
        }
    }
}
