﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Services.Security;
namespace Nop.Web.Framework.Mvc.Filters
{
    /// <summary>
    /// 简化权限语句。
    /// 使用方法;
    /// [Permission(PermissionSystemNames.Systems.Departments.Create)]
    /// 将如上代码放到Action上
    /// </summary>
    public class PermissionAttribute : TypeFilterAttribute
    {
        #region Fields

        private readonly bool _ignoreFilter;

        #endregion

        #region Ctor

        /// <summary>
        /// Create instance of the filter attribute
        /// </summary>
        /// <param name="ignore">Whether to ignore the execution of filter actions</param>
        public PermissionAttribute(string permissions, bool ignore = false) : base(typeof(PermissionFilter))
        {
            Permissions = permissions ?? throw new ArgumentNullException("permissions");
            _ignoreFilter = ignore;
            Arguments = new object[] { ignore };
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether to ignore the execution of filter actions
        /// </summary>
        public bool IgnoreFilter => _ignoreFilter;

        public string Permissions { get; }

        #endregion


        #region Nested filter

        /// <summary>
        /// Represents a filter that confirms access to the admin panel
        /// </summary>
        private class PermissionFilter : IAuthorizationFilter
        {
            #region Fields

            private readonly bool _ignoreFilter;
            private readonly IPermissionService _permissionService;

            #endregion

            #region Ctor

            public PermissionFilter(bool ignoreFilter, IPermissionService permissionService)
            {
                _ignoreFilter = ignoreFilter;
                _permissionService = permissionService;
            }

            #endregion

            #region Methods

            /// <summary>
            /// Called early in the filter pipeline to confirm request is authorized
            /// </summary>
            /// <param name="filterContext">Authorization filter context</param>
            public void OnAuthorization(AuthorizationFilterContext filterContext)
            {
                if (filterContext == null)
                    throw new ArgumentNullException(nameof(filterContext));

                //check whether this filter has been overridden for the action
                var actionFilter = filterContext.ActionDescriptor.FilterDescriptors
                    .Where(filterDescriptor => filterDescriptor.Scope == FilterScope.Action)
                    .Select(filterDescriptor => filterDescriptor.Filter).OfType<PermissionAttribute>().FirstOrDefault();

                //ignore filter (the action is available even if a customer hasn't access to the admin area)
                if (actionFilter?.IgnoreFilter ?? _ignoreFilter)
                    return;

                if (!DataSettingsManager.DatabaseIsInstalled)
                    return;

                var permissions = actionFilter.Permissions;
                if (!_permissionService.Authorize(permissions))
                {
                    if(filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                    {
                        filterContext.Result = new JsonResult(new
                        {
                            error = "您无权访问当前功能"
                        });  
                    }
                    else
                    {
                        //filterContext.Result = new ChallengeResult();
                        //filterContext.Result = new ForbidResult();
                        var webHelper = EngineContext.Current.Resolve<IWebHelper>();
                        filterContext.Result = new AccessDeniedResult(actionName: "AccessDenied",
                            controllerName: "Security",
                            routeValues: new { area = "Admin", pageUrl = webHelper.GetRawUrl(filterContext.HttpContext.Request) });
                    }
                }
                    

                ////there is AdminAuthorizeFilter, so check access
                //if (filterContext.Filters.Any(filter => filter is PermissionFilter))
                //{
                //    //authorize permission of access to the admin area
                //    if (!_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel))
                //        filterContext.Result = new ChallengeResult();
                //}
            }

            #endregion
        }

        #endregion
    }
}
